# (A) Project title
DHRISHTI(Visual Aiding for Outdoor Obstacles)

## Project Advisor: Swaminathan

## Project Mentor: Geetha M

## Team:
  1. AM.EN.U4CSE16008   Amruda Kandoth
  2. AM.EN.U4CSE16014   Arya NR
  3. AM.EN.U4CSE16148   Ritika Pillai
  4. AM.EN.U4CSE16150   Priya TV 



# (B) Problem:
  - There have been innumerable researches conducted to improve navigation services for the blind when they are moving through an outdoor space.In this project we propose a data model to help visually impaired to safely navigate in outdoor spaces.



## Context of your work
  - Health and Lifestyle  --> Deep Learning, Image Processing, Internet of Things --> Obstacle Detection,Semantic Interpretation, Computer Vision --> Visual Aiding for Outdoor Obstacles(DHRISHTI)
  - Computer Vision, Semantic Interpretation, Single Shot Environment Navigation, Audio interface, Object Detection ,Dimensions, Distance, 
    Non-contact distance, Pedestrian detection, Image-to-Speech, Visually Impaired, Obstacle avoidance, Navigation, Scene Understanding, Video Analytics, 

   

## Background
  - foundation - three important bases for the formal 
  - [Discussion] What problem(s) do they address + pros and cons of their solution?
  - [Motivation] How did the above papers influence your problem definition? 
  > (in about 3 paragraphs)

## Brief problem description
  - Informal description of your problem
  - Formal statement of your problem
  - Additional information pertaining to the problem
  > (in about 300 words, clear and specific)

## Relevance
  1. Who will benefit from your work?
  2. How will they use your work?
  > (in about 100 words, without muddling)

## Novelty
  1. What is unique about your work? Is it the problem or solution approach or any other? Substantiate.
  2. What distinguishes your work from other related work?
  > (in 2 paragraphs)

**Note**: Don't make tall claims without substantiating them

## Scope
  - Justify your problem is neither too big nor too small but of adequate size for a final year project
  > (in one paragraph)

## Expected contribution
  - Comprehensive survey?
  - New algorithm?
  - Improving existing algorithm?
  - Comparative study?
  - Experimental study?
  > (in clear and unambiguous terms)
  
## Data set
  - What is the source of your data set?
  - Justify your data set is reliable
  - Justify your data set is adequate
  - How do you plan to eliminate noise in your data set?

---

# (C) Solution
**This part should be completed by Sep 16, 2019**

## Approach
  - Terms and definitions
  - Discuss your solution approach in complete detail
  - Justify credibility and appropriateness of your solution

## Block Diagram
  - Detailed block diagram with inputs, outputs, modules - stagewise, intermediate inputs and outputs
  - Every block and every arrow should be annotated
  - The block diagram should be in sync with solution approach

## Algorithm(s)
  - Provide the algorithm(s), connect it with solution approach
  - Discuss the correctness and complexity
  - Provide a running example demonstrating the working of the algorithm
  - Discuss working of algorithm for unusual and corner cases

## Implementation
  - [Source code](https://github.com/username/projectname/blob/master/source/....)

  > (point to your github source code repository)

---

# (D) Experiments

**This part should be completed by Oct 31, 2019**

## Evaluation strategy
  - What is your plan of evaluation?
  - Elaborate your test setup
  - What parameters are you going to measure?
  - Justify these parameters are relevant and adequate
  - How do you plan to report your results?
  - Does your evaluation plan include large experiments?
  - Justify your evaluation strategy is sound

## Data set
  - Define the format of your dataset (or test data)
  - Provide a descrition of your fields in your dataset
  - Provide a description of how you plan to use your dataset
  - Justify your data set is sufficient for basing your experiments

## Results
  - What are your results?
  - How will you report your results? (charts/table/...)
  - Justify your results are valid
  - Justify your results are reliable

## Interpretation of results
  - What do your results say?
  - Do your results in line with your project goal?
  - Do your results include false positives/false negatives?

---

# References
  - author names, title of the paper, conference/journal, year
  > (in standard format)

