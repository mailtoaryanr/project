# Weekly status report

---

## Week of Jun 17

### Phase
  - [X] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Completed this
  2. Working on that ....

### Issues/Concerns/Road blocks
  1. Unable to configure Eclipse...
  2. Installation issue with ....

### Action plan to address issues
  1. Query in Eclipse forum
  2. Blah.. blah ..

### Plans for the next week
  1. Start work on ....
  2. Continue work on ...

### Comments from guide (very specific)
  - Satisfactory progress has been made
  - Try the following suggestions to resolve issue
    1. Do this ..
    2. Do that ..
  
---

## Week of Jun 24

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Understanding the scope of the project
  2. Finding Reasearch papers related to the project 

### Issues/Concerns/Road blocks
  1. Finding Novelty for the issue
  2. Doubts about to what extent the project can be implemented

### Action plan to address issues
  1. To find and research efficient algorithms
  2. We are currently concentrating the project to create a system that works outdoor.

### Plans for the next week
  1. Reading the Research papers
  2. Creating a block diagram for the project

### Comments from guide (very specific)
  1. Rasearch more about algorithms related to each module(semantic interpretation,scene understanding,video analytics)
  2. Prepare a block diagram consisting of the various features.

---

## Week of Jul 1

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Read research papers to get the knowledge about the pre-exiting algorithms.
  2. Prepared a block diagram consisting of the description and features in the project.

### Issues/Concerns/Road blocks
  1. We found that there are a lot of pre-existing models and choosing an efficient one was a major challenge.


### Action plan to address issues
  1. Our aim is to make a fast, efficient model to help the visually impaired in the outdoor environment.
  2. So we have decided to research in depth to achieve our goal.

### Plans for the next week
  1. Find an algorithm for object detection 
  2. Read object detection related papers. 

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Jul 15

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. We read about various algorithm for object detection 
  2. We began with two step detection algorithms that is CNN and R-CNN. 

### Issues/Concerns/Road blocks
  1. We studied that RCNN takes a lot of time for each region, making it inefficient  for real time environment.
  2. We also found that these networks do not look at the complete image, rather it look at parts of it which have the highest probability of containing the objects

### Action plan to address issues
  1. We decided to further look into other algorithms available.

### Plans for the next week
  1. Look up more object detection algorithm.

### Comments from guide (very specific)
  1. ...
  2. ...


---



---

## Week of Aug 5

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. We continued reading about various algorithm for object detection.
  2. We began with two step detection algorithms that is Fast R-CNN and Faster R-CNN.

### Issues/Concerns/Road blocks
  1. Fast R-CNN during testing time and in regions slows down the algorithm significantly.

### Action plan to address issues
  1. We decided to further look into other algorithms available that is unified detection algorithm.

### Plans for the next week
  1. Read about unified detection algorithm such as YOLO and SSD.

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 12

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. We read about YOLO(You Only Look Once) and SSD.
  2. We comapared the efficiency and speed of YOLO and SSD

### Issues/Concerns/Road blocks
  1. YOLO was not as efficient as SSD but it had speed and choosing which choosing between efficiency and speed was an issue.

### Action plan to address issues
  1. We chose speed over effciency because according to the project speed was more important.

### Plans for the next week
  1. Reading elaborately about YOLO and decciding which platform to use it.

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 19

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. We  decided to use YOLO as the algorithm for object detection and read more about it.
  2. We studied about yolo and how its used.

### Issues/Concerns/Road blocks
  1. Since we were going to use yolo,we had to sacrifice accuracy


### Plans for the next week
  1. Collecting dataset about the project topic.

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 26

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. We searched for the dataset pertaining to our project topic.

### Issues/Concerns/Road blocks
  1. We had issues finding the right dataset that follows the white cane's height and angle for getting accurate results.

### Action plan to address issues
  1. On further reasearch we found the dataset according to the factors.

### Plans for the next week
  1. Implementing YOLO and the dataset.

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 2

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 9

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Implemented YOLO using the current dataset.

### Issues/Concerns/Road blocks
  1. To get 

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 16

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 23

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 30

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Oct 7

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Oct 14

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Oct 21

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Oct 28

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

